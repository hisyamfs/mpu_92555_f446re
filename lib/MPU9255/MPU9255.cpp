#include "MPU9255.h"
#include "mbed.h"

MPU9255::MPU9255(PinName sda, PinName scl) :
_imu(sda, scl)
{
    configMPU(PWR_MGMT_1, 1 << 7); // setting bit H_RESET ke 1, agar semua register MPU9255 kembali ke nilai default
    configAK(AK8963_CNTL2, 1); // reset semua register AK8963 ke nilai default
    configMPU(PWR_MGMT_1, 0); // PWR_MGMT_1 mode default : tidak sleep mode, tidak cycle mode, tidak gyro standby mode, clock dari internal 20MHz Oscillator
    configMPU(PWR_MGMT_2, 0); // PWR_MGMT_2 bernilai 0x00, untuk memastikan semua sumbu accel dan gyro menyala
    configMPU(USER_CTRL, 0); // disable I2C Master module, sehingga data magnetometer ak8963 dibypass langsung ke nucleo
    configMPU(INT_PIN_CFG, 0x02); // set INT_PIN_CFG ke bypass mode
    configAK(AK8963_CNTL, 0x12); // Setting magnetometer agar dalam mode contionus measurement 1, dengan output 16 bit
    setGyroFullScale(0); // setting skala pembacaan penuh gyro ke 250 dps
    setAccelFullScale(0); // setting skala pembacaan penuh accel ke 2 g
    for (int i = 0; i < 3; ++i) {
        _rmag[i] = 0;
        _rgyro[i] = 0;
        _raccel[i] = 0;
        _mag[i] = 0.0f;
        _gyro[i] = 0.0f;
        _accel[i] = 0.0f;
        _mag_hard_bias[i] = 0.0f;
        _mag_soft_bias[i] = 1;
    }
}

void MPU9255::configMPU(int reg, char config) {
    char bfr[2] = {(char) reg, config};
    _imu.write(MPU9255_ADDRESS, bfr, 2);
}

void MPU9255::configAK(int reg, char config) {
    char bfr[2] = {(char) reg, config};
    _imu.write(AK8963_ADDRESS, bfr, 2);
}

void MPU9255::readMPU(int reg, char* dest, int leng) {
    char bfr = (char) reg;
    _imu.write(MPU9255_ADDRESS, &bfr, 1);
    _imu.read(MPU9255_ADDRESS, dest, leng);
}

void MPU9255::readAK(int reg, char* dest, int leng) {
    char bfr = (char) reg;
    _imu.write(AK8963_ADDRESS, &bfr, 1);
    _imu.read(AK8963_ADDRESS, dest, leng);
}

void MPU9255::updateRawGyro() {
    char bfr[6];
    readMPU(GYRO_XOUT_H, bfr, 6);

    _rgyro[0] = (int16_t) bfr[0] << 8 | bfr[1]; // X
    _rgyro[1] = (int16_t) bfr[2] << 8 | bfr[3]; // Y
    _rgyro[2] = (int16_t) bfr[4] << 8 | bfr[5]; // Z
}

void MPU9255::updateRawAccel() {
    char bfr[6];
    readMPU(ACCEL_XOUT_H, bfr, 6);

    _raccel[0] = (int16_t) bfr[0] << 8 | bfr[1]; // X
    _raccel[1] = (int16_t) bfr[2] << 8 | bfr[3]; // Y
    _raccel[2] = (int16_t) bfr[4] << 8 | bfr[5]; // Z
}

void MPU9255::updateRawMag() {
    char bfr[7];
    readAK(AK8963_XOUT_L, bfr, 7); // 7 byte, register AK8963_ST2 = AK8963_XOUT_L + 6 harus dibaca agar magnetometer melakukan lagi pengukuran

    _rmag[0] = (int16_t) bfr[1] << 8 | bfr[0]; // X
    _rmag[1] = (int16_t) bfr[3] << 8 | bfr[2]; // Y
    _rmag[2] = (int16_t) bfr[5] << 8 | bfr[4]; // Z
}

void MPU9255::setGyroFullScale(int mode) {
    if (mode >= 0 && mode <= 3) { // mode hanya dari 0 s.d. 3. Jika parameter diluar itu, tidak lakukan apa apa.
        char fs_sel = (char) mode << 3; // bit FS_SEL adalah bit ke 4 dan ke 3 dari register GYRO_CONFIG.
        configMPU(GYRO_CONFIG, fs_sel);
        switch (mode) { // lihat MPU 9255 product description halaman 8. data mentah dibagi dengan sensitivity scale factor, tergatung mode yang dipilih.
            case 0  : _gyro_multiplier = 1.0f/131.0f ; break; // max 250 dps
            case 1  : _gyro_multiplier = 1.0f/65.5f  ; break; // max 500 dps
            case 2  : _gyro_multiplier = 1.0f/32.8f  ; break; // max 1000 dps
            case 3  : _gyro_multiplier = 1.0f/16.4f  ; break; // max 2000 dps
            default : _gyro_multiplier = 1.0f        ; break; // invalid
        }
    }
}

float MPU9255::getGyroMultiplier() {
    float K;
    char fs_sel;
    readMPU(GYRO_CONFIG, &fs_sel);
    fs_sel = (fs_sel & 0x18) >> 3; // hanya baca bit ke 4 dan ke 3, lalu geser 3 ke kanan
    switch (fs_sel) {// lihat MPU 9255 product description halaman 8. data mentah dibagi dengan sensitivity scale factor, tergatung mode yang dipilih.
        case 0  : K = 1.0f/131.0f ; break; // max 250 dps
        case 1  : K = 1.0f/65.5f  ; break; // max 500 dps
        case 2  : K = 1.0f/32.8f  ; break; // max 1000 dps
        case 3  : K = 1.0f/16.4f  ; break; // max 2000 dps
        default : K = 1.0f        ; break; // invalid
    }
    return K;
}

void MPU9255::setAccelFullScale(int mode) {
    if (mode >= 0 && mode <= 3) {
        char fs_sel = (char) mode << 3; // bit FS_SEL adalah bit ke 4 dan ke 3 dari register ACCEL_CONFIG
        configMPU(ACCEL_CONFIG, fs_sel);
        switch (mode) { // lihat MPu9255 product desc hal.9. sama seperti pada gyro.
            case 0  : _accel_mult = 1.0f / 16384.0f ; break; // 2g
            case 1  : _accel_mult = 1.0f / 8192.0f  ; break; // 4g
            case 2  : _accel_mult = 1.0f / 4096.0f  ; break; // 8g
            case 3  : _accel_mult = 1.0f / 2048.0f  ; break; // 16g
            default : _accel_mult = 1.0f            ; break; // invalid
        }
    }
}

float MPU9255::getAccelMultiplier() {
    float K; char fs_sel;
    readMPU(ACCEL_CONFIG, &fs_sel);
    fs_sel = (fs_sel & 0x18) >> 3; // hanya baca bit ke 4 dan 3. geser ke kanan 3 untuk memperoleh angka 0 s.d. 3
    switch (fs_sel) { // lihat MPu9255 product desc hal.9. sama seperti pada gyro.
        case 0  : K = 1.0f / 16384.0f ; break; // 2g
        case 1  : K = 1.0f / 8192.0f  ; break; // 4g
        case 2  : K = 1.0f / 4096.0f  ; break; // 8g
        case 3  : K = 1.0f / 2048.0f  ; break; // 16g
        default : K = 1.0f            ; break; // invalid
    }
    return K;
}

void MPU9255::updateMag() {
    for (int i = 0; i < 3; ++i) {
        _mag[i] = ((_rmag[i] * 0.15f) - _mag_hard_bias[i]) * _mag_soft_bias[i];
    }
}

void MPU9255::updateGyro() {
    for (int i = 0; i < 3; ++i) {
        _gyro[i] = _rgyro[i] * _gyro_multiplier;
    }
}

void MPU9255::updateAccel() {
    for (int i = 0; i < 3; ++i) {
        _accel[i] = _raccel[i] * _accel_mult;
    }
}

void MPU9255::setInitYaw() {
    updateRawMag();
    updateMag();
    updateYawMag();
    _yaw_c = _yaw_m;
}

void MPU9255::updateRollPitch() {
    float &ax = _accel[0], &ay = _accel[1], &az = _accel[2];
    _pitch = atan2f(ax, sqrt(ax*ax+az*az));
    _roll = atan2f(ay,az);
}

void MPU9255::updateYawMag() {
    float &mx = _mag[0], &my = _mag[1], &mz = _mag[2];
    float hx = mx*cosf(_pitch) + mz*sinf(_pitch);
    float hy = mx*sinf(_roll)*sinf(_pitch) + my*cosf(_roll) - mz*sinf(_roll)*cosf(_pitch);
    if (hx == 0 && hy == 0) { // handle NaN
        return;
    } else {
        _yaw_m = atan2f(hx, hy) * 180.0f / 3.14f;
    }
}

void MPU9255::updateCompFilter(const float &a, const float &dt) {
    float delta_yaw = -_gyro[2] * dt; // gz*dt
    _yaw_c = a*(_yaw_c+delta_yaw) + (1-a)*_yaw_m;
}

float MPU9255::getYawFilter() {
    return _yaw_c;
}

void MPU9255::getRollPitch(float &roll, float &pitch) {
    roll = _roll;
    pitch = _pitch;
}

float MPU9255::getYawMag() {
    return _yaw_m;
}

void MPU9255::setMagHardBias(const float &x, const float &y, const float &z) {
    _mag_hard_bias[0] = x;
    _mag_hard_bias[1] = y;
    _mag_hard_bias[2] = z;
}

void MPU9255::setMagSoftBias(const float &x, const float &y, const float &z) {
    _mag_soft_bias[0] = x;
    _mag_soft_bias[1] = y;
    _mag_soft_bias[2] = z;
}

void MPU9255::setGyroOffset(const float &x, const float &y, const float &z) {
    int16_t ox, oy, oz; // offset pada sumbu x, y, z dalam bentuk int16_t
    char bfr[6]; // data yang akan dikirim ke register XG_OFFSET_H
    float K = 32.8f; // di datasheet, format data untuk offset register adalah dalam full scale +- 1000 dps
    ox = (int16_t) (- K * x); // ubah ke int16_t
    oy = (int16_t) (- K * y);
    oz = (int16_t) (- K * z);
    
    bfr[0] = (ox >> 8) & 0xFF; // xh
    bfr[1] = ox & 0xFF; // xl
    bfr[2] = (oy >> 8) & 0xFF; // y
    bfr[3] = oy & 0xFF;
    bfr[4] = (oz >> 8) & 0xFF; // z
    bfr[5] = oz & 0xFF;

    configMPU(XG_OFFSET_H, bfr[0]);
    configMPU(XG_OFFSET_L, bfr[1]);
    configMPU(YG_OFFSET_H, bfr[2]);
    configMPU(YG_OFFSET_L, bfr[3]);
    configMPU(ZG_OFFSET_H, bfr[4]);
    configMPU(ZG_OFFSET_L, bfr[5]);
}

void MPU9255::setAccelOffset(const float &x, const float &y, const float &z) {
    int16_t ox, oy, oz; // offset pada sumbu x, y, z dalam bentuk int16_t
    int16_t mask = 1; // untuk mengecek apakah bit ke 0 ox, oy, oz dari OTP bernilai 1
    char bfr[6]; // data yang akan dikirim ke register XA_OFFSET_H
    char mask_bit[3] = {0, 0, 0}; // agar bit ke 0 ox, oy, oz dari OTP tidak berubah
    float K = 2048.0f; // di datasheet, format data untuk offset register adalah dalam full scale +- 16g
    // baca dulu nilai register yang sudah disimpan
    getAccelOffset(ox, oy, oz);
    // baca bit ke 0 dari ox, oy, oz apakah bernilai 1. Jika ya, set mask_bit ke 1
    if (ox & mask) {
        mask_bit[0] = 0x01;
    }
    if (oy & mask) {
        mask_bit[1] = 0x01;
    }
    if (oz & mask) {
        mask_bit[2] = 0x01;
    }

    ox += (int16_t) (- K * x); // ubah x ke int16_t, lalu tambah ke nilai yang sudah ada di register
    oy += (int16_t) (- K * y);
    oz += (int16_t) (- K * z);
    
    bfr[0] = (ox >> 8) & 0xFF; // xh
    bfr[1] = ox & 0xFF; // xl
    bfr[1] = bfr[1] | mask_bit[0]; // preserve bit 0 of ox
    bfr[2] = (oy >> 8) & 0xFF; // y
    bfr[3] = oy & 0xFF;
    bfr[3] = bfr[3] | mask_bit[1];
    bfr[4] = (oz >> 8) & 0xFF; // z
    bfr[5] = oz & 0xFF;
    bfr[5] = bfr[5] | mask_bit[2];

    configMPU(XA_OFFSET_H, bfr[0]);
    configMPU(XA_OFFSET_L, bfr[1]);
    configMPU(YA_OFFSET_H, bfr[2]);
    configMPU(YA_OFFSET_L, bfr[3]);
    configMPU(ZA_OFFSET_H, bfr[4]);
    configMPU(ZA_OFFSET_L, bfr[5]);
}

void MPU9255::getAccelOffset(int16_t &x, int16_t &y, int16_t &z) {
    char bfr[6];
    readMPU(XA_OFFSET_H, bfr    , 2);
    readMPU(YA_OFFSET_H, &bfr[2], 2);
    readMPU(ZA_OFFSET_H, &bfr[4], 2);
    x = ((int16_t) bfr[0] << 8) | bfr[1];
    y = ((int16_t) bfr[2] << 8) | bfr[3];
    z = ((int16_t) bfr[4] << 8) | bfr[5];
}

void MPU9255::getRawGyroData(int16_t &x, int16_t &y, int16_t &z) {
    x = _rgyro[0];
    y = _rgyro[1];
    z = _rgyro[2];
}

void MPU9255::getRawAccelData(int16_t &x, int16_t &y, int16_t &z) {
    x = _raccel[0];
    y = _raccel[1];
    z = _raccel[2];
}

void MPU9255::getRawMagData(int16_t &x, int16_t &y, int16_t &z) {
    x = _rmag[0];
    y = _rmag[1];
    z = _rmag[2];
}

void MPU9255::getMag(float &x, float &y, float &z) {
    x = _mag[0];
    y = _mag[1];
    z = _mag[2];
}

void MPU9255::getGyro(float &x, float &y, float &z) {
    x = _gyro[0];
    y = _gyro[1];
    z = _gyro[2];
}

void MPU9255::getAccel(float &x, float &y, float &z) {
    x = _accel[0];
    y = _accel[1];
    z = _accel[2];
}