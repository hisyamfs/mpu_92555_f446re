#ifndef MBED_MPU9255_H
#define MBED_MPU9255_H

#include "mbed.h"

#define MPU9255_ADDRESS 0x68 << 1
#define AK8963_ADDRESS 0x0C << 1

#define AK8963_WIA       0x00 // should return 0x48
#define AK8963_INFO      0x01
#define AK8963_ST1       0x02  // data ready status bit 0
#define AK8963_XOUT_L	 0x03  // data
#define AK8963_XOUT_H	 0x04
#define AK8963_YOUT_L	 0x05
#define AK8963_YOUT_H	 0x06
#define AK8963_ZOUT_L	 0x07
#define AK8963_ZOUT_H	 0x08
#define AK8963_ST2       0x09  // Data overflow bit 3 and data read error status bit 2
#define AK8963_CNTL      0x0A  // Power down (0000), single-measurement (0001), self-test (1000) and Fuse ROM (1111) modes on bits 3:0
#define AK8963_CNTL2     0x0B  // Saat bit ke 0 bernilai 1 , maka AK8963 akan melakukan soft reset
#define AK8963_ASTC      0x0C  // Self test control
#define AK8963_I2CDIS    0x0F  // I2C disable
#define AK8963_ASAX      0x10  // Fuse ROM x-axis sensitivity adjustment value
#define AK8963_ASAY      0x11  // Fuse ROM y-axis sensitivity adjustment value
#define AK8963_ASAZ      0x12  // Fuse ROM z-axis sensitivity adjustment value

#define SELF_TEST_X_GYRO 0x00
#define SELF_TEST_Y_GYRO 0x01
#define SELF_TEST_Z_GYRO 0x02

#define X_FINE_GAIN      0x03 // [7:0] fine gain
#define Y_FINE_GAIN      0x04
#define Z_FINE_GAIN      0x05
#define XA_OFFSET_H_TC   0x06 // User-defined trim values for accelerometer
#define XA_OFFSET_L_Set   0x07
#define YA_OFFSET_H_Set  0x08
#define YA_OFFSET_L_Set   0x09
#define ZA_OFFSET_H_Set  0x0A
#define ZA_OFFSET_L_   0x0B

#define SELF_TEST_X_ACCEL 0x0D
#define SELF_TEST_Y_ACCEL 0x0E
#define SELF_TEST_Z_ACCEL 0x0F

#define SELF_TEST_A      0x10

#define XG_OFFSET_H      0x13  // User-defined trim values for gyroscope
#define XG_OFFSET_L      0x14
#define YG_OFFSET_H      0x15
#define YG_OFFSET_L      0x16
#define ZG_OFFSET_H      0x17
#define ZG_OFFSET_L      0x18
#define SMPLRT_DIV       0x19
#define CONFIG           0x1A
#define GYRO_CONFIG      0x1B
#define ACCEL_CONFIG     0x1C
#define ACCEL_CONFIG2    0x1D
#define LP_ACCEL_ODR     0x1E
#define WOM_THR          0x1F

#define MOT_DUR          0x20  // Duration counter threshold for motion interrupt generation, 1 kHz rate, LSB = 1 ms
#define ZMOT_THR         0x21  // Zero-motion detection threshold bits [7:0]
#define ZRMOT_DUR        0x22  // Duration counter threshold for zero motion interrupt generation, 16 Hz rate, LSB = 64 ms

#define FIFO_EN          0x23
#define I2C_MST_CTRL     0x24
#define I2C_SLV0_ADDR    0x25
#define I2C_SLV0_REG     0x26
#define I2C_SLV0_CTRL    0x27
#define I2C_SLV1_ADDR    0x28
#define I2C_SLV1_REG     0x29
#define I2C_SLV1_CTRL    0x2A
#define I2C_SLV2_ADDR    0x2B
#define I2C_SLV2_REG     0x2C
#define I2C_SLV2_CTRL    0x2D
#define I2C_SLV3_ADDR    0x2E
#define I2C_SLV3_REG     0x2F
#define I2C_SLV3_CTRL    0x30
#define I2C_SLV4_ADDR    0x31
#define I2C_SLV4_REG     0x32
#define I2C_SLV4_DO      0x33
#define I2C_SLV4_CTRL    0x34
#define I2C_SLV4_DI      0x35
#define I2C_MST_STATUS   0x36
#define INT_PIN_CFG      0x37
#define INT_ENABLE       0x38
#define DMP_INT_STATUS   0x39  // Check DMP interrupt
#define INT_STATUS       0x3A
#define ACCEL_XOUT_H     0x3B
#define ACCEL_XOUT_L     0x3C
#define ACCEL_YOUT_H     0x3D
#define ACCEL_YOUT_L     0x3E
#define ACCEL_ZOUT_H     0x3F
#define ACCEL_ZOUT_L     0x40
#define TEMP_OUT_H       0x41
#define TEMP_OUT_L       0x42
#define GYRO_XOUT_H      0x43
#define GYRO_XOUT_L      0x44
#define GYRO_YOUT_H      0x45
#define GYRO_YOUT_L      0x46
#define GYRO_ZOUT_H      0x47
#define GYRO_ZOUT_L      0x48
#define EXT_SENS_DATA_00 0x49
#define EXT_SENS_DATA_01 0x4A
#define EXT_SENS_DATA_02 0x4B
#define EXT_SENS_DATA_03 0x4C
#define EXT_SENS_DATA_04 0x4D
#define EXT_SENS_DATA_05 0x4E
#define EXT_SENS_DATA_06 0x4F
#define EXT_SENS_DATA_07 0x50
#define EXT_SENS_DATA_08 0x51
#define EXT_SENS_DATA_09 0x52
#define EXT_SENS_DATA_10 0x53
#define EXT_SENS_DATA_11 0x54
#define EXT_SENS_DATA_12 0x55
#define EXT_SENS_DATA_13 0x56
#define EXT_SENS_DATA_14 0x57
#define EXT_SENS_DATA_15 0x58
#define EXT_SENS_DATA_16 0x59
#define EXT_SENS_DATA_17 0x5A
#define EXT_SENS_DATA_18 0x5B
#define EXT_SENS_DATA_19 0x5C
#define EXT_SENS_DATA_20 0x5D
#define EXT_SENS_DATA_21 0x5E
#define EXT_SENS_DATA_22 0x5F
#define EXT_SENS_DATA_23 0x60
#define MOT_DETECT_STATUS 0x61
#define I2C_SLV0_DO      0x63
#define I2C_SLV1_DO      0x64
#define I2C_SLV2_DO      0x65
#define I2C_SLV3_DO      0x66
#define I2C_MST_DELAY_CTRL 0x67
#define SIGNAL_PATH_RESET  0x68
#define MOT_DETECT_CTRL  0x69
#define USER_CTRL        0x6A  // Bit 7 enable DMP, bit 3 reset DMP
#define PWR_MGMT_1       0x6B // Device defaults to the SLEEP mode
#define PWR_MGMT_2       0x6C
#define DMP_BANK         0x6D  // Activates a specific bank in the DMP
#define DMP_RW_PNT       0x6E  // Set read/write pointer to a specific start address in specified DMP bank
#define DMP_REG          0x6F  // Register in DMP from which to read or to which to write
#define DMP_REG_1        0x70
#define DMP_REG_2        0x71
#define FIFO_COUNTH      0x72
#define FIFO_COUNTL      0x73
#define FIFO_R_W         0x74
#define WHO_AM_I_MPU9255 0x75 // Should return 0x73
#define XA_OFFSET_H      0x77
#define XA_OFFSET_L      0x78
#define YA_OFFSET_H      0x7A
#define YA_OFFSET_L      0x7B
#define ZA_OFFSET_H      0x7D
#define ZA_OFFSET_L      0x7E

/** Library untuk membaca data dari MPU9255
 * 
 * Example:
 * @code
 * #include "mbed.h"
 * #include "MPU9255.h"
 * 
 * MPU9255 mpu(D14, D15);
 * Serial pc(USBTX, USBRX);
 * float mx, my, mz; // magnetometer data
 * float gx, gy, gz; // gyro data
 * float ax, ay, az; // accel data
 * float yaw;
 * Timer update_timer, print_timer;
 * 
 * float hard_bias[3] = {-12.375f, 52.8f, -4.125f};
 * float soft_bias[3] = {0.939219443f,	0.986559158f, 1.084996292f};
 * const float gyro_offset[3] = {-1.184750631f, 0.925087344f, 0.914914601f};
 * const float accel_offset[3] = {-0.049580148f, -0.029021086f, 0.06124163f};
 * 
 * int main() {
 *      mpu.setMagHardBias(hard_bias[0], hard_bias[1], hard_bias[2]);
 *      mpu.setMagSoftBias(soft_bias[0], soft_bias[1], soft_bias[2]);
 *      mpu.setGyroOffset(gyro_offset[0], gyro_offset[1], gyro_offset[2]);
 *      mpu.setInitYaw();
 *      
 *      update_timer.start();
 *      print_timer.start();
 *      while (1) {
 *          if (update_timer.read() >= 0.01) { // update tiap 0.01 detik
 *              mpu.updateRawGyro();
 *              mpu.updateRawMag();
 *              mpu.updateRawAccel();
 *              mpu.updateMag();
 *              mpu.updateGyro();
 *              mpu.updateAccel();
 *              
 *              mpu.updateRollPitch();
 *              mpu.updateYawMag();
 *              mpu.updateCompFilter(0.95f, 0.01f);
 * 
 *              mpu.getMag(mx, my, mz);
 *              mpu.getGyro(gx, gy, gz);
 *              mpu.getAccel(ax, ay, az);
 *              yaw = mpu.getYawFilter();
 *              
 *              update_timer.reset();
 *          }
 *          if (print_timer.read() >= 0.5) { // print tiap 0.5 detik
 *              pc.printf("yaw=%.1f ", yaw);
 *              pc.printf("mx=%.1f my=%.1f mz=%.1f ", mx, my, mz);
 *              pc.printf("gx=%.1f gy=%.1f gz=%.1f ", gx, gy, gz);
 *              pc.printf("ax=%.1f ay=%.1f az=%.1f \n", ax, ay, az);
 *              
 *              print_timer.reset();
 *          }
 *      }
 * }
 * @endcode
 */

class MPU9255 {
public:
    /** Membuat objek MPU9255 dengan pin I2C yang dipilih
     * @param sda pin sda yang disambung ke sensor
     * @param sck pin scl yang disambung ke sensor
     * 
     * @note 
     *      Skala penuh gyro dan accel dipilih 250 derajat/s dan 2g
     *      pada konstruktor
     */
    MPU9255(PinName sda, PinName scl);
    
    /** Menulis data 1 byte ke register di MPU (accel dan gyro)
     * @param reg register MPU9255 yang akan ditulis
     * @param config data yang akan ditulis ke register
     */
    void configMPU(int reg, char config);

    /** Menulis 1 byte data ke register di AK8963 (magnetometer)
     * @param reg register AK8963 yang dituju
     * @param config data yang akan ditulis ke register
     */
    void configAK(int reg, char config);

    /** Membaca data dari register MPU (accel dan gyro)
     * @param reg register yang akan dibaca datanya
     * @param dest pointer ke byte-array yang menyimpan data yang dikirim
     * @param leng jumlah byte yang akan dibaca
     */
    void readMPU(int reg, char* dest, int leng = 1);

    /** Membaca data dari register AK8963 (magnetometer)
     * @param reg register yang akan dibaca datanya
     * @param dest pointer ke byte-array yang menyimpan data yang dikirim
     * @param leng jumlah byte yang akan dibaca
     */
    void readAK(int reg, char* dest, int leng = 1);
    
    /** Mengupdate data mentah gyro (_raw_gyro[3]) yang disimpan objek ke data terbaru dari sensor
     */
    void updateRawGyro();

    /** Mengupdate data mentah magnetometer (_raw_mag[3]) yang disimpan objek ke data terbaru dari sensor
     */
    void updateRawMag();

    /** Mengupdate data mentah accel yang disimpan objek ke data terbaru dari sensor
     */
    void updateRawAccel();

    /** Mengatur skala penuh yang dapat diukur gyro, dan mengubah _gyro_multiplier ke
     * nilai yang sesuai dengan skala yang dipilih
     * 
     * @param mode angka antara 0 sampai 3, dengan :
     *          0 : 250 derajat/s
     *          1 : 500 derajat/s
     *          2 : 1000 derajat/s
     *          3 : 2000 derajat/s
     * 
     * @note Jika angka yang dimasukkan tidak sesuai, fungsi ini tidak akan melakukan apa apa
     * @note Fungsi ini dipanggil pada constructor, dengan parameter mode = 0 atau skala penuh 250 derajat/s
     */
    void setGyroFullScale(int mode = 0);

    /** Memperoleh angka yang harus dikali ke data mentah gyro untuk mendapatakan data dalam
     * satuan derajat/s
     * 
     * @return angka yang harus dikali ke data mentah gyro untuk mendapatakan data dalam
     * satuan derajat/s
     */
    float getGyroMultiplier();

    /** Mengatur skala penuh yang dapat diukur accelerometer, dan mengubah _accel_multiplier
     *  ke nilai yang sesuai
     * 
     * @param mode int antara 0 sampi 3 , dengan :
     *      0 : 2g
     *      1 : 4g
     *      2 : 8g
     *      3 : 16g
     * 
     * @note Jika int yang dimasukkan tidak sesuai, fungsi ini tidak melakukan apa apa.
     * @note Pada constructor, fungsi ini dipanggil dengan mode = 0, atau secara default skala penuh accel 2g.
     */
    void setAccelFullScale(int mode = 0);

    /** Memperoleh angka yang harus dikalikan ke data mentah accel (_raw_accel[3]) untuk memperoleh
     * data dalam satuan g (_accel[3])
     * 
     * @return angka yang harus dikalikan ke data mentah accel (_raw_accel[3]) untuk memperoleh
     * data dalam satuan g (_accel[3])
     */
    float getAccelMultiplier();

    /** Mengupdate data magnetometer dalam uT (_mag[3]) berdasarkan data mentah magnetometer yang terakhir disimpan
     * (_raw_mag[3])
     */
    void updateMag();

    /** Mengupdate data gyro dalam derajat/s (_gyro[3]) berdasarkan data mentah gyro yang terakhir disimpan
     * (_raw_gyro[3])
     */
    void updateGyro();

    /** Mengupdate data accel dalam g (_accel[3]) berdasarkan data mentah accel yang terakhir disimpan 
     * (_raw_accel[3])
     */
    void updateAccel();

    /** Memperoleh nilai pertama yaw untuk complementary filter
     * @note diasumsikan semua nilai offset kalibrasi sudah diatur sebelum fungsi ini dipanggil
     * @note diasumsikan fungsi ini hanya dipanggil sekali, di awal program.
     */
    void setInitYaw();

    /** Mengupdate nilai roll dan pitch (dalam radian) berdasarkan pembacaan terakhir accelerometer yang disimpan di objek
     *  
     * @note belum memperhitungkan singularitas, gimbal lock, etc.
     */
    void updateRollPitch();

    /** Mengupdate nilai yaw dari magnetometer berdasarkan nilai terakhir data magnetometer, roll, dan pitch.
     * 
     * @note sudah dengan tilt compensation
     */
    void updateYawMag();

    /** Mengupdate nilai yaw (sudut dari utara) berdasarkan nilai terakhir gyro, dan yaw magnetometer
     * 
     * @param a nilai alpha untuk complementary filter
     * @param dt selang waktu semenjak update terakhir
     * 
     * @note rumus complementary filter : y' = a * y + (1-a) * x
     */
    void updateCompFilter(const float &a, const float& dt);

    /** Memperoleh nilai yaw terakhir 
     * 
     * @return yaw terakhir yang dihitung complementary filter, dalam satuan derajat
     */
    float getYawFilter();

    /** Memperoleh nilai terakhir roll dan pitch yang disimpan
     * 
     * @param roll variabel yang akan menyimpan nilai roll daam rad
     * @param pitch variabel yang akan menyimpan nilai pitch dalam rad
     */
    void getRollPitch(float &roll, float &pitch);

    /** Memperoleh nilai terakhir yaw yang hanya dihitung dari data mag dan accel
     * @return nilai terakhir yaw magnetometer, dalam derajat
     */
    float getYawMag();
    
    /** Mengatur nilai hard iron bias yang akan disimpan objek
     * @param x Hard iron bias di sumbu x, satuan uT
     * @param y Hard iron bias di sumbu y, satuan uT
     * @param z Hard iron bias di sumbu z, satuan uT
     * 
     * @note rumus untuk memperoleh data magnetometer :
     * = ((data mentah * 0.15) - hard iron bias) * soft iron bias
     * @note untuk keterangan lebih lanjut : https://github.com/kriswiner/MPU6050/wiki/Simple-and-Effective-Magnetometer-Calibration
     */
    void setMagHardBias(const float &x, const float &y, const float &z);

    /** Mengatur nilai soft iron bias yang akan disimpan objek
     * @param x soft iron bias di sumbu x
     * @param y soft iron bias di sumbu y
     * @param z soft iron bias di sumbu z
     * 
     * @note rumus untuk memperoleh data magnetometer :
     *     = ((data mentah * 0.15) - hard iron bias) * soft iron bias
     * @note untuk keterangan lebih lanjut : https://github.com/kriswiner/MPU6050/wiki/Simple-and-Effective-Magnetometer-Calibration
     */
    void setMagSoftBias(const float &x, const float &y, const float &z);

    /** Mengatur nilai offset gyro dan menyimpannya di register offset gyro.
     * 
     * @param x Offset gyro di sumbu x, dalam satuan derajat/s
     * @param y Offset gyro di sumbu y, dalam satuan derajat/s
     * @param z Offset gyro di sumbu z, dalam satuan derajat/s
     * 
     * @note data yang dibaca dari updateRawGyro() sudah disesuaikan dengan nilai offset. Penyesuaian terjadi di sensor
     */
    void setGyroOffset(const float &x, const float &y, const float &z);

    /** Mengatur nilai offset accel dan menyimpannya di register offset accel
     * 
     * @param x Offset accel di sumbu x, dalam satuan g
     * @param y Offset accel di sumbu y, dalam satuan g
     * @param z Offset accel di sumbu z, dalam satuan g
     * 
     * @note data yang dibaca dari updateRawAccel() sudah disesuaikan dengan nilai offset. Penyesuaian terjadi di sensor
     * @note sebelum fungsi ini dipanggil, register offset accel sudah memiliki nilai sendiri. Nilai tersebut akan ditambah dengan nilai dari parameter fungsi ini
     */
    void setAccelOffset(const float &x, const float &y, const float &z);

    /** Memperoleh nilai register offset accel di sumbu x, y, dan z
     * 
     * @param x variable yang menyimpan nilai offset di sumbu x
     * @param y variable yang menyimpan nilai offset di sumbu y
     * @param z variable yang menyimpan nilai offset di sumbu z
     * 
     * @note fungsi ini dipanggil di setAccelOffset
     */
    void getAccelOffset(int16_t &x, int16_t &y, int16_t &z);
    
    /** Memperoleh data mentah gyro yang terakhir disimpan objek
     * @param x variabel yang menyimpan data mentah gyro terakhir di sumbu x
     * @param y variabel yang menyimpan data mentah gyro terakhir di sumbu y
     * @param z variabel yang menyimpan data mentah gyro terakhir di sumbu z
     */
    void getRawGyroData(int16_t &x, int16_t &y, int16_t &z);

    /** Memperoleh data mentah accel yang terakhir disimpan objek
     * @param x variabel yang menyimpan data mentah accel terakhir di sumbu x
     * @param y variabel yang menyimpan data mentah accel terakhir di sumbu y
     * @param z variabel yang menyimpan data mentah accel terakhir di sumbu z
     */
    void getRawAccelData(int16_t &x, int16_t &y, int16_t &z);

    /** Memperoleh data mentah magnetometer yang terakhir disimpan objek
     * @param x variabel yang menyimpan data mentah magnetometer terakhir di sumbu x
     * @param y variabel yang menyimpan data mentah magnetometer terakhir di sumbu y
     * @param z variabel yang menyimpan data mentah magnetometer terakhir di sumbu z
     */
    void getRawMagData(int16_t &x, int16_t &y, int16_t &z);

    /** Memperoleh data magnetometer yang terakhir disimpan objek
     * @param x variabel yang menyimpan data sumbu x, satuan uT
     * @param y variabel yang menyimpan data sumbu y, satuan uT
     * @param z variabel yang menyimpan data sumbu z, satuan uT
     */
    void getMag(float &x, float &y, float &z);

    /** Memperoleh data gyro yang terakhir disimpan objek
     * @param x variabel yang menyimpan data sumbu x, satuan derajat/s
     * @param y variabel yang menyimpan data sumbu y, satuan derajat/s
     * @param z variabel yang menyimpan data sumbu z, satuan derajat/s
     */    
    void getGyro(float &x, float &y, float &z);

    /** Memperoleh data acceleromter yang terakhir disimpan objek
     * @param x variabel yang menyimpan data sumbu x, satuan g.
     * @param y variabel yang menyimpan data sumbu y, satuan g.
     * @param z variabel yang menyimpan data sumbu z, satuan g.
     */
    void getAccel(float &x, float &y, float &z);
protected:
    I2C _imu;
    // magnetometer
    int16_t _rmag[3]; // data mentah dari magnetometer sb.x, y, dan z
    float _mag[3]; // data yang telah diubah dalam satuan uT
    float _mag_hard_bias[3]; float _mag_soft_bias[3]; // hard iron dan soft iron bias magnetometer
    // gyroscope
    int16_t _rgyro[3]; // data mentah dari gyro sb.x, y, dan z
    float _gyro[3]; // data mentah yang telah diubah ke satuan derajat/s
    float _gyro_multiplier; // angka yang dikalikan dengn _rgyro untuk memperoleh _gyro
    // accelerometer
    int16_t _raccel[3]; // data mentah dari accel sb x, y, z
    float _accel[3]; // data mentah yang telah diubah ke satuan g(gravity)
    float _accel_mult; // angka yang dikali denga _raccel untuk memperoleh _accel
    // complementary filter
    float _roll, _pitch;
    float _yaw_m; // yaw murni dari magnetometer saja
    float _yaw_c; // yaw dari complementary filter
};

#endif