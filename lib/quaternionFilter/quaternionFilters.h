#ifndef _QUATERNIONFILTERS_H_
#define _QUATERNIONFILTERS_H_

#include "mbed.h"

/** Library untuk madgwick filter MPU9255.
 *  Paper untuk filter ini dapat didownload pada http://x-io.co.uk/open-source-imu-and-ahrs-algorithms/
 *  Berdasarkan kode kris winer yang dapat dilihat pada https://github.com/kriswiner/MPU9250/blob/master/STM32F401/MPU9250.h, dengan sedikit perubahan. 
 *  Penjelasan singkat dapat dilihat pada http://www.olliw.eu/2013/imu-data-fusing/#chapter11
 * 
 *  Example:
 *  @code
 *  #include "mbed.h"
 *  #include "MPU9255.h"
 *  #include "quaternionFilters.h"
 *  
 *  #define DEG_TO_RAD 0.017453292519943295769236907684886f
 *  #define RAD_TO_DEG 57.295779513082320876798154814105f
 *  
 *  MPU9255 mpu(D14, D15); // sda, scl
 *  Serial pc(USBTX, USBRX); // tx, rx
 *  float mx, my, mz;
 *  float gx, gy, gz;
 *  float ax, ay, az;
 *  
 *  float roll = 0.0f, pitch = 0.0f, yaw = 0.0f;
 *  
 *  Timer update_timer, print_timer;
 *  
 *  float hard_bias[3] = {-12.375f, 52.8f, -4.125f};
 *  float soft_bias[3] = {0.939219443f,	0.986559158f, 1.084996292f};
 *  
 *  const float gyro_offset[3] = {-1.184750631f, 0.925087344f, 0.914914601f};
 *  
 *  const float accel_offset[3] = {-0.049580148f, -0.029021086f, 0.06124163f};
 *  
 *  int main() {
 *      mpu.setMagHardBias(hard_bias[0], hard_bias[1], hard_bias[2]);
 *      mpu.setMagSoftBias(soft_bias[0], soft_bias[1], soft_bias[2]);
 *      mpu.setGyroOffset(gyro_offset[0], gyro_offset[1], gyro_offset[2]);
 *      mpu.setAccelOffset(accel_offset[0], accel_offset[1], accel_offset[2]);
 *  
 *      update_timer.start();
 *      print_timer.start();
 *  
 *      while (1) {
 *          if (update_timer.read() >= 0.01) {
 *              mpu.updateRawGyro();
 *              mpu.updateRawMag();
 *              mpu.updateRawAccel();
 *          
 *              mpu.updateMag();
 *              mpu.updateGyro();
 *              mpu.updateAccel();
 * 
 *              mpu.getMag(mx, my, mz);
 *              mpu.getGyro(gx, gy, gz);
 *              mpu.getAccel(ax, ay, az);
 * 
 *              // Sensors x (y)-axis of the accelerometer/gyro is aligned with the y (x)-axis of the magnetometer;
 *              // the magnetometer z-axis (+ down) is misaligned with z-axis (+ up) of accelerometer and gyro!
 *              // We have to make some allowance for this orientation mismatch in feeding the output to the quaternion filter.
 *              // For the MPU9250+MS5637 Mini breakout the +x accel/gyro is North, then -y accel/gyro is East. So if we want te quaternions properly aligned
 *              // we need to feed into the Madgwick function Ax, -Ay, -Az, Gx, -Gy, -Gz, My, -Mx, and Mz. But because gravity is by convention
 *              // positive down, we need to invert the accel data, so we pass -Ax, Ay, Az, Gx, -Gy, -Gz, My, -Mx, and Mz into the Madgwick
 *              // function to get North along the accel +x-axis, East along the accel -y-axis, and Down along the accel -z-axis.
 *              // This orientation choice can be modified to allow any convenient (non-NED) orientation convention.
 *              // Pass gyro rate as rad/s
 *              MadgwickQuaternionUpdate(-ax, ay, az, 
 *                                      gx * DEG_TO_RAD, -gy * DEG_TO_RAD, -gz * DEG_TO_RAD,
 *                                      my, -mx, mz,
 *                                      0.01f            
 *              );
 *              getRPY(roll, pitch, yaw);
 *              yaw *= RAD_TO_DEG;
 *              pitch *= RAD_TO_DEG;
 *              roll *= RAD_TO_DEG;
 *  
 *              update_timer.reset();
 *          }
 *  
 *          if (print_timer.read() >= 0.5) {
 *              pc.printf("yaw = %f pitch = %f roll = %f \n", yaw, pitch, roll);
 *  
 *              print_timer.reset();
 *          }
 *      }
 *  }
 *  @endcode
 *  
 */

void MadgwickQuaternionUpdate(float ax, float ay, float az, float gx, float gy,
                              float gz, float mx, float my, float mz,
                              float deltat);

const float * getQ();

// fungsi untuk memperoleh euler angle dengan aerospace convention. Satuan dalam radian.
void getRPY(float &roll, float &pitch, float &yaw);

#endif // _QUATERNIONFILTERS_H_
