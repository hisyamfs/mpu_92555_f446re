#include "mbed.h"
#include "MPU9255.h"
#include "quaternionFilters.h"

#define DEG_TO_RAD 0.017453292519943295769236907684886f
#define RAD_TO_DEG 57.295779513082320876798154814105f

MPU9255 mpu(D14, D15); // sda, scl
Serial pc(USBTX, USBRX); // tx, rx
float mx, my, mz;
float gx, gy, gz;
float ax, ay, az;
char mpu_id = 0x00, ak_id = 0x00, PWR_MGMT_2_stat = 0x2C;

float yaw_c = 0.0f; // yaw dari complementary filter
float yaw_m, pitch_m, roll_m;; // dari madgwick filter

Timer update_timer, print_timer;

float hard_bias[3] = {-12.375f, 52.8f, -4.125f};
float soft_bias[3] = {0.939219443f,	0.986559158f, 1.084996292f};

const float gyro_offset[3] = {-1.184750631f, 0.925087344f, 0.914914601f};

const float accel_offset[3] = {-0.049580148f, -0.029021086f, 0.06124163f};

int main() {
    mpu.setMagHardBias(hard_bias[0], hard_bias[1], hard_bias[2]);
    mpu.setMagSoftBias(soft_bias[0], soft_bias[1], soft_bias[2]);
    mpu.setGyroOffset(gyro_offset[0], gyro_offset[1], gyro_offset[2]);
    mpu.setAccelOffset(accel_offset[0], accel_offset[1], accel_offset[2]);
    mpu.setInitYaw();

    update_timer.start();
    print_timer.start();

    while (1) {
        if (update_timer.read() >= 0.01) {
            mpu.updateRawGyro();
            mpu.updateRawMag();
            mpu.updateRawAccel();
            mpu.updateMag();
            mpu.updateGyro();
            mpu.updateAccel();

            mpu.getMag(mx, my, mz);
            mpu.getGyro(gx, gy, gz);
            mpu.getAccel(ax, ay, az);
            mpu.readMPU(WHO_AM_I_MPU9255, &mpu_id);
            mpu.readAK(AK8963_WIA, &ak_id);
            mpu.readMPU(PWR_MGMT_2, &PWR_MGMT_2_stat);

            // update complementary filter
            mpu.updateRollPitch();
            mpu.updateYawMag();
            mpu.updateCompFilter(0.99f, 0.01f);
            yaw_c = mpu.getYawFilter();

            // update madgwick filter
            MadgwickQuaternionUpdate(-ax, ay, az, 
                                    gx * DEG_TO_RAD, -gy * DEG_TO_RAD, -gz * DEG_TO_RAD,
                                    my, -mx, mz,
                                    0.01f            
            );
            getRPY(roll_m, pitch_m, yaw_m);
            yaw_m *= RAD_TO_DEG;
            
            update_timer.reset();
        }

        if (print_timer.read() >= 0.5) {
            pc.printf("yaw comp = %.1f yaw madgwick = %.1f \n", yaw_c, yaw_m);
            // pc.printf("mx=%.1f my=%.1f mz=%.1f \n", mx, my, mz);
            // pc.printf("gx=%.1f gy=%.1f gz=%.1f \n", gx, gy, gz);
            // pc.printf("ax=%.1f ay=%.1f az = %.1f \n", ax, ay, az);
            // pc.printf("mpu_id(73)=%X ak_id(48)=%X PWR_MGMT_2_stat=%X \n", mpu_id, ak_id, PWR_MGMT_2_stat);

            print_timer.reset();
        }
    }
}